﻿using Drains.Web.Data.Contracts.Repositories;
using Drains.Web.Data.EntityFramework.DTO.Adress;
using System.Collections.Generic;
using Drains.Web.Data.EntityFramework.Repositories.Core;
using Drains.Web.Data.EntityFramework.Entities;
using Drains.Web.Data.Contracts.DTO.Adress;
using System;
using System.Linq.Expressions;
using System.Linq;

namespace Drains.Web.Data.EntityFramework.Repositories
{
    public class AdressRepository : BaseRepository<DataAdress, Adress, long>, IAdressRepository
    {
        public long Create(string region, string district, string location)
        {
            var entity = base.Create(new DataAdress()
            {
                District = district,
                Region = region,
                Location = location
            });
            return entity.Id;
        }

        public bool Delete(long id)
        {
            return base.Remove(id);
        }

        public IDataAdress GetAdressById(long id)
        {
            using (var context = GetContext())
            {
                var entity = context.Adresses.FirstOrDefault(GetId(id));
                if (entity != null)
                {
                    return new DataAdress
                    {
                        Id = entity.Id,
                        District = entity.District,
                        Location = entity.Location,
                        Region = entity.Region
                    };
                }
            };
            return null;
        }

        public IEnumerable<IDataAdress> GetAll()
        {
            using (var context = GetContext())
            {
                var entity = context.Adresses.Select(i =>
                    new DataAdress
                    {
                        Id = i.Id,
                        District = i.District,
                        Region = i.Region,
                        Location = i.Location
                    }).ToList();
                return entity.AsEnumerable();
            };
        }

        public bool Update(long id, string region, string district, string location)
        {
            return base.Update(new DataAdress
            {
                Id = id,
                Region = region,
                District = district,
                Location = location
            });
        }

        protected override IEnumerable<DataAdress> ConvertToModel(IEnumerable<Adress> entities)
        {
            var list = entities.Select(x => new DataAdress
            {
                Id = x.Id,
                District = x.District,
                Location = x.Location,
                Region = x.Region
            }).ToList();

            return list;
        }

        protected override void GetEntity(DataAdress dataModel, ref Adress entity)
        {
            entity.Id = dataModel.Id;
            entity.Location = dataModel.Location;
            entity.District = dataModel.District;
            entity.Region = dataModel.Region;
        }

        protected override Expression<Func<Adress, bool>> GetId(DataAdress dataModel)
        {
            Expression<Func<Adress, bool>> expression = (x => x.Id == dataModel.Id);
            return expression;
        }

        protected override Expression<Func<Adress, bool>> GetId(long key)
        {
            Expression<Func<Adress, bool>> expression = (x => x.Id == key);
            return expression;
        }
    }
}
