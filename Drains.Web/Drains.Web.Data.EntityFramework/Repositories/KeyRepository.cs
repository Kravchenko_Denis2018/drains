﻿using Drains.Web.Data.Contracts.DTO.Key;
using Drains.Web.Data.Contracts.Repositories;
using Drains.Web.Data.EntityFramework.DTO.Key;
using Drains.Web.Data.EntityFramework.Entities;
using Drains.Web.Data.EntityFramework.Repositories.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.Repositories
{
    public class KeyRepository : BaseRepository<DataKey, Keys, long>, IKeyRepository
    {
        public long Create(long personalId, int numberKey, long supplierId)
        {
            var entity = base.Create(new DataKey()
            {
                NumberKey = numberKey,
                PersonalId = personalId,
                SupplierId = supplierId
            });
            return entity.Id;
        }

        public bool Delete(long id)
        {
            return base.Remove(id);
        }

        public IEnumerable<IDataKey> GetAll()
        {
            using (var context = GetContext())
            {
                var entity = context.Keys.Select(i =>
                    new DataKey
                    {
                        Id = i.Id,
                        PersonalId = i.PersonalId,
                        NumberKey = i.NumberKey,
                        SupplierId = i.PersonalId
                    }).ToList();
                return entity.AsEnumerable();
            };
        }

        public IDataKey GetKeyById(long id)
        {
            using (var context = GetContext())
            {
                var entity = context.Keys.FirstOrDefault(GetId(id));
                if (entity != null)
                {
                    return new DataKey
                    {
                        Id = entity.Id,
                        PersonalId = entity.PersonalId,
                        NumberKey = entity.NumberKey,
                        SupplierId = entity.SupplierId
                    };
                }
            };
            return null;
        }

        public bool Update(long id, long personalId, int numberKey, long supplierId)
        {
            return base.Update(new DataKey
            {
                Id = id,
                PersonalId = personalId,
                NumberKey = numberKey,
                SupplierId = supplierId
            });
        }

        protected override IEnumerable<DataKey> ConvertToModel(IEnumerable<Keys> entities)
        {
            var list = entities.Select(x => new DataKey
            {
                Id = x.Id,
                PersonalId = x.PersonalId,
                NumberKey = x.NumberKey,
                SupplierId = x.SupplierId
            }).ToList();

            return list;
        }

        protected override void GetEntity(DataKey dataModel, ref Keys entity)
        {
            entity.Id = dataModel.Id;
            entity.PersonalId = dataModel.PersonalId;
            entity.NumberKey = dataModel.NumberKey;
            entity.SupplierId = dataModel.SupplierId;
        }

        protected override Expression<Func<Keys, bool>> GetId(DataKey dataModel)
        {
            Expression<Func<Keys, bool>> expression = (x => x.Id == dataModel.Id);
            return expression;
        }

        protected override Expression<Func<Keys, bool>> GetId(long key)
        {
            Expression<Func<Keys, bool>> expression = (x => x.Id == key);
            return expression;
        }
    }
}
