﻿using Drains.Web.Data.Contracts.DTO.Contract;
using Drains.Web.Data.Contracts.Repositories;
using Drains.Web.Data.EntityFramework.DTO.Contract;
using Drains.Web.Data.EntityFramework.Repositories.Core;
using Drains.Web.Data.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Drains.Web.Data.EntityFramework.Repositories
{
    public class ContractRepository : BaseRepository<DataContract, Entities.Contracts, long>, IContractRepository
    {
        public long Create(int number, DateTime dateNow, DateTime dateEnd, long supplierId)
        {
            var entity = base.Create(new DataContract()
            {
                Number = number,
                DateNow = dateNow,
                DateEnd = dateEnd,
                SupplierId = supplierId
            });
            return entity.Id;
        }

        public bool Delete(long id)
        {
            return base.Remove(id);
        }

        public IEnumerable<IDataContract> GetAll()
        {
            using (var context = GetContext())
            {
                var entity = context.Contracts.Select(i =>
                    new DataContract
                    {
                        Id = i.Id,
                        Number = i.Number,
                        DateNow = i.DateNow,
                        DateEnd = i.DateEnd,
                        SupplierId = i.SupplierId
                    }).ToList();
                return entity.AsEnumerable();
            };
        }

        public IDataContract GetContractById(long id)
        {
            using (var context = GetContext())
            {
                var entity = context.Contracts.FirstOrDefault(GetId(id));
                if (entity != null)
                {
                    return new DataContract
                    {
                        Id = entity.Id,
                        Number = entity.Number,
                        DateNow = entity.DateNow,
                        DateEnd = entity.DateEnd,
                        SupplierId = entity.SupplierId
                    };
                }
            };
            return null;
        }

        public bool Update(long id, int number, DateTime dateNow, DateTime dateEnd, long supplierId)
        {
            return base.Update(new DataContract
            {
                Id = id,
                Number = number,
                DateNow = dateNow,
                DateEnd = dateEnd,
                SupplierId = supplierId
            });
        }

        protected override IEnumerable<DataContract> ConvertToModel(IEnumerable<Entities.Contracts> entities)
        {
            var list = entities.Select(x => new DataContract
            {
                Id = x.Id,
                Number = x.Number,
                DateNow = x.DateNow,
                DateEnd = x.DateEnd,
                SupplierId = x.SupplierId
            }).ToList();

            return list;
        }

        protected override void GetEntity(DataContract dataModel, ref Entities.Contracts entity)
        {
            entity.Id = dataModel.Id;
            entity.Number = dataModel.Number;
            entity.DateNow = dataModel.DateNow;
            entity.DateEnd = dataModel.DateEnd;
            entity.SupplierId = dataModel.SupplierId;
        }

        protected override Expression<Func<Entities.Contracts, bool>> GetId(DataContract dataModel)
        {
            Expression<Func<Entities.Contracts, bool>> expression = (x => x.Id == dataModel.Id);
            return expression;
        }

        protected override Expression<Func<Entities.Contracts, bool>> GetId(long key)
        {
            Expression<Func<Entities.Contracts, bool>> expression = (x => x.Id == key);
            return expression;
        }
    }
}
