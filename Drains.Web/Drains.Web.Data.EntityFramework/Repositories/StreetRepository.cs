﻿using Drains.Web.Data.Contracts.DTO.Street;
using Drains.Web.Data.Contracts.Repositories;
using Drains.Web.Data.EntityFramework.DTO.Street;
using Drains.Web.Data.EntityFramework.Entities;
using Drains.Web.Data.EntityFramework.Repositories.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.Repositories
{
    public class StreetRepository : BaseRepository<DataStreet, Streets, long>, IStreetRepository
    {
        public long Create(string street, int postcode, int adressId)
        {
            var entity = base.Create(new DataStreet()
            {
                Street = street,
                Postcode = postcode,
                AdressId = adressId
            });
            return entity.Id;
        }

        public bool Delete(long id)
        {
            return base.Remove(id);
        }

        public IEnumerable<IDataStreet> GetAll()
        {
            using (var context = GetContext())
            {
                var entity = context.Streets.Select(i =>
                    new DataStreet
                    {
                        Id = i.Id,
                        Street = i.Street,
                        Postcode = i.Postcode,
                        AdressId = i.AdressId
                    }).ToList();
                return entity.AsEnumerable();
            };
        }

        public IDataStreet GetStreetById(long id)
        {
            using (var context = GetContext())
            {
                var entity = context.Streets.FirstOrDefault(GetId(id));
                if (entity != null)
                {
                    return new DataStreet
                    {
                        Id = entity.Id,
                        Postcode = entity.Postcode,
                        Street = entity.Street,
                        AdressId = entity.AdressId
                    };
                }
            };
            return null;
        }

        public bool Update(long id, string street, int postcode, int adressId)
        {
            return base.Update(new DataStreet
            {
                Id = id,
                Street = street,
                Postcode = postcode,
                AdressId = adressId
            });
        }

        protected override IEnumerable<DataStreet> ConvertToModel(IEnumerable<Streets> entities)
        {
            var list = entities.Select(x => new DataStreet
            {
                Id = x.Id,
                Street = x.Street,
                Postcode = x.Postcode,
                AdressId = x.AdressId
            }).ToList();

            return list;
        }

        protected override void GetEntity(DataStreet dataModel, ref Streets entity)
        {
            entity.Id = dataModel.Id;
            entity.Street = dataModel.Street;
            entity.Postcode = dataModel.Postcode;
            entity.AdressId = dataModel.AdressId;
        }

        protected override Expression<Func<Streets, bool>> GetId(DataStreet dataModel)
        {
            Expression<Func<Streets, bool>> expression = (x => x.Id == dataModel.Id);
            return expression;
        }

        protected override Expression<Func<Streets, bool>> GetId(long key)
        {
            Expression<Func<Streets, bool>> expression = (x => x.Id == key);
            return expression;
        }
    }
}
