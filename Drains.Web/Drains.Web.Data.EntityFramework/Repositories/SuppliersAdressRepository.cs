﻿using Drains.Web.Data.Contracts.DTO.SuppliersAdress;
using Drains.Web.Data.Contracts.Repositories;
using Drains.Web.Data.EntityFramework.DTO.SuppliersAdress;
using Drains.Web.Data.EntityFramework.Entities;
using Drains.Web.Data.EntityFramework.Repositories.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.Repositories
{
    public class SuppliersAdressRepository : BaseRepository<DataSuppliersAdress, SuppliersAdress, long>, ISuppliersAdressRepository
    {
        public long Create(long suppliersCode, string build, string apartment, int phone1, int? phone2, int? phone3, string email1, string email2, string nameSupplier, long streetId)
        {
            var entity = base.Create(new DataSuppliersAdress()
            {
                SuppliersCode = suppliersCode,
                Build = build,
                Apartment = apartment,
                Phone1 = phone1,
                Phone2 = phone2,
                Phone3 = phone3,
                Email1 = email1,
                Email2 = email2,
                NameSupplier = nameSupplier,
                StreetId = streetId
            });
            return entity.Id;
        }

        public bool Delete(long id)
        {
            return base.Remove(id);
        }

        public IEnumerable<IDataSuppliersAdress> GetAll()
        {
            using (var context = GetContext())
            {
                var entity = context.SuppliersAdresses.Select(i =>
                    new DataSuppliersAdress
                    {
                        Id = i.Id,
                        SuppliersCode = i.SuppliersCode,
                        Build = i.Build,
                        Apartment = i.Apartment,
                        Phone1 = i.Phone1,
                        Phone2 = i.Phone2,
                        Phone3 = i.Phone3,
                        Email1 = i.Email1,
                        Email2 = i.Email2,
                        NameSupplier = i.NameSupplier,
                        StreetId = i.StreetId
                    }).ToList();
                return entity.AsEnumerable();
            };
        }

        public IDataSuppliersAdress GetSuppliersAdressById(long id)
        {
            using (var context = GetContext())
            {
                var entity = context.SuppliersAdresses.FirstOrDefault(GetId(id));
                if (entity != null)
                {
                    return new DataSuppliersAdress
                    {
                        Id = entity.Id,
                        SuppliersCode = entity.SuppliersCode,
                        Build = entity.Build,
                        Apartment = entity.Apartment,
                        Phone1 = entity.Phone1,
                        Phone2 = entity.Phone2,
                        Phone3 = entity.Phone3,
                        Email1 = entity.Email1,
                        Email2 = entity.Email2,
                        NameSupplier = entity.NameSupplier,
                        StreetId = entity.StreetId
                    };
                }
            };
            return null;
        }

        public bool Update(long id, long suppliersCode, string build, string apartment, int phone1, int? phone2, int? phone3, string email1, string email2, string nameSupplier, long streetId)
        {
            return base.Update(new DataSuppliersAdress
            {
                Id = id,
                SuppliersCode = suppliersCode,
                Build = build,
                Apartment = apartment,
                Phone1 = phone1,
                Phone2 = phone2,
                Phone3 = phone3,
                Email1 = email1,
                Email2 = email2,
                NameSupplier = nameSupplier,
                StreetId = streetId
            });
        }

        protected override IEnumerable<DataSuppliersAdress> ConvertToModel(IEnumerable<SuppliersAdress> entities)
        {
            var list = entities.Select(x => new DataSuppliersAdress
            {
                Id = x.Id,
                SuppliersCode = x.SuppliersCode,
                Build = x.Build,
                Apartment = x.Apartment,
                Phone1 = x.Phone1,
                Phone2 = x.Phone2,
                Phone3 = x.Phone3,
                Email1 = x.Email1,
                Email2 = x.Email2,
                NameSupplier = x.NameSupplier,
                StreetId = x.StreetId
            }).ToList();

            return list;
        }

        protected override void GetEntity(DataSuppliersAdress dataModel, ref SuppliersAdress entity)
        {
            entity.Id = dataModel.Id;
            entity.SuppliersCode = dataModel.SuppliersCode;
            entity.Build = dataModel.Build;
            entity.Apartment = dataModel.Apartment;
            entity.Phone1 = dataModel.Phone1;
            entity.Phone2 = dataModel.Phone2;
            entity.Phone3 = dataModel.Phone3;
            entity.Email1 = dataModel.Email1;
            entity.Email2 = dataModel.Email2;
            entity.NameSupplier = dataModel.NameSupplier;
            entity.StreetId = dataModel.StreetId;
        }

        protected override Expression<Func<SuppliersAdress, bool>> GetId(DataSuppliersAdress dataModel)
        {
            Expression<Func<SuppliersAdress, bool>> expression = (x => x.Id == dataModel.Id);
            return expression;
        }

        protected override Expression<Func<SuppliersAdress, bool>> GetId(long key)
        {
            Expression<Func<SuppliersAdress, bool>> expression = (x => x.Id == key);
            return expression;
        }
    }
}
