﻿
namespace Drains.Web.Data.EntityFramework.Repositories.Core
{
    public abstract class CoreRepository
    {
        protected DrainsStorageModel GetContext()
        {
            return new DrainsStorageModel();
        }
    }
}
