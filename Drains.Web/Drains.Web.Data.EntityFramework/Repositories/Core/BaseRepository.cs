﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.Repositories.Core
{
    /// <summary>
    /// Base repository class
    /// </summary>
    /// <typeparam name="TA">Data adapter class</typeparam>
    /// <typeparam name="TE">Entity class</typeparam>
    /// <typeparam name="TKey">Type of Key of Entity class</typeparam>
    /// <typeparam name="TR">Repository class</typeparam>
    public abstract class BaseRepository<TA, TE, TKey> : CoreRepository
        where TA : class, new()
        where TE : class, new()
    {

        #region Abstract methods
        protected abstract void GetEntity(TA dataModel, ref TE entity);
        protected abstract Expression<Func<TE, Boolean>> GetId(TA dataModel);
        protected abstract Expression<Func<TE, Boolean>> GetId(TKey key);
        protected abstract IEnumerable<TA> ConvertToModel(IEnumerable<TE> entities);
        #endregion

        #region Private methods


        private TE GetEntity(TA dataModel)
        {
            var result = new TE();
            GetEntity(dataModel, ref result);
            return result;
        }

        #endregion

        #region CRUD Methods

        protected TE Create(TA record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            using (var context = GetContext())
            {
                TE entity = GetEntity(record);
                context.Set<TE>().Add(entity);
                context.SaveChanges();
                return entity;
            }
        }

        protected bool Update(TA record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            using (var context = GetContext())
            {
                var entity = context.Set<TE>().Where(GetId(record)).FirstOrDefault();
                GetEntity(record, ref entity);
                return context.SaveChanges() > 0;
            }
        }

        protected bool Remove(TKey key)
        {
            using (var context = GetContext())
            {
                var entity = context.Set<TE>().Where(GetId(key)).FirstOrDefault();
                if (entity != null)
                {
                    context.Set<TE>().Remove(entity);
                    return context.SaveChanges() > 0;
                }
            }
            return false;
        }
        #endregion

        protected IEnumerable<TA> GetWithFilter(Expression<Func<TE, bool>> expresion)
        {
            using (var context = GetContext())
            {
                var entity = context.Set<TE>().Where(expresion).Cast<TE>().ToList<TE>();
                var result = ConvertToModel(entity);

                return result;
            }
        }
    }
}
