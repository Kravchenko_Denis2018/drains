﻿using Drains.Web.Data.Contracts.DTO.Personal;
using Drains.Web.Data.Contracts.Repositories;
using Drains.Web.Data.EntityFramework.DTO.Personal;
using Drains.Web.Data.EntityFramework.Entities;
using Drains.Web.Data.EntityFramework.Repositories.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.Repositories
{
    public class PersonalRepository : BaseRepository<DataPersonal, Personals, long>, IPersonalRepository
    {
        public long Create(string surname, string name, string middleName, int phone1, int? phone2, string email, long supplierId)
        {
            var entity = base.Create(new DataPersonal()
            {
                Surname = surname,
                Name = name,
                MiddleName = middleName,
                Phone1 = phone1,
                Phone2 = phone2,
                Email = email,
                SupplierId = supplierId
            });
            return entity.Id;
        }

        public bool Delete(long id)
        {
            return base.Remove(id);
        }

        public IEnumerable<IDataPersonal> GetAll()
        {
            using (var context = GetContext())
            {
                var entity = context.Personals.Select(i =>
                    new DataPersonal
                    {
                        Id = i.Id,
                        Surname = i.Surname,
                        Name = i.Name,
                        MiddleName = i.MiddleName,
                        Phone1 = i.Phone1,
                        Phone2 = i.Phone2,
                        Email = i.Email,
                        SupplierId = i.SupplierId
                    }).ToList();
                return entity.AsEnumerable();
            };
        }

        public IDataPersonal GetPersonalById(long id)
        {
            using (var context = GetContext())
            {
                var entity = context.Personals.FirstOrDefault(GetId(id));
                if (entity != null)
                {
                    return new DataPersonal
                    {
                        Id = entity.Id,
                        Surname = entity.Surname,
                        Name = entity.Name,
                        MiddleName = entity.MiddleName,
                        Phone1 = entity.Phone1,
                        Phone2 = entity.Phone2,
                        Email = entity.Email,
                        SupplierId = entity.SupplierId
                    };
                }
            };
            return null;
        }

        public bool Update(long id, string surname, string name, string middleName, int phone1, int? phone2, string email, long supplierId)
        {
            return base.Update(new DataPersonal
            {
                Id = id,
                Surname = surname,
                Name = name,
                MiddleName = middleName,
                Phone1 = phone1,
                Phone2 = phone2,
                Email = email,
                SupplierId = supplierId
            });
        }

        protected override IEnumerable<DataPersonal> ConvertToModel(IEnumerable<Personals> entities)
        {
            var list = entities.Select(x => new DataPersonal
            {
                Id = x.Id,
                Surname = x.Surname,
                Name = x.Name,
                MiddleName = x.MiddleName,
                Phone1 = x.Phone1,
                Phone2 = x.Phone2,
                Email = x.Email,
                SupplierId = x.SupplierId
            }).ToList();

            return list;
        }

        protected override void GetEntity(DataPersonal dataModel, ref Personals entity)
        {
            entity.Id = dataModel.Id;
            entity.Surname = dataModel.Surname;
            entity.Name = dataModel.Name;
            entity.MiddleName = dataModel.MiddleName;
            entity.Phone1 = dataModel.Phone1;
            entity.Phone2 = dataModel.Phone2;
            entity.Email = dataModel.Email;
            entity.SupplierId = dataModel.SupplierId;
        }

        protected override Expression<Func<Personals, bool>> GetId(DataPersonal dataModel)
        {
            Expression<Func<Personals, bool>> expression = (x => x.Id == dataModel.Id);
            return expression;
        }

        protected override Expression<Func<Personals, bool>> GetId(long key)
        {
            Expression<Func<Personals, bool>> expression = (x => x.Id == key);
            return expression;
        }
    }
}
