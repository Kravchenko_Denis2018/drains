namespace Drains.Web.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Adress",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Region = c.String(nullable: false, maxLength: 150),
                        District = c.String(nullable: false, maxLength: 150),
                        Location = c.String(nullable: false, maxLength: 150),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        DateNow = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                        SupplierId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SuppliersAdress", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.SuppliersAdress",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SuppliersCode = c.Long(nullable: false),
                        Build = c.String(nullable: false),
                        Apartment = c.String(nullable: false),
                        Phone1 = c.Int(nullable: false),
                        Phone2 = c.Int(),
                        Phone3 = c.Int(),
                        Email1 = c.String(nullable: false),
                        Email2 = c.String(),
                        NameSupplier = c.String(),
                        StreetId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Streets", t => t.StreetId, cascadeDelete: true)
                .Index(t => t.StreetId);
            
            CreateTable(
                "dbo.Streets",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Street = c.String(nullable: false, maxLength: 150),
                        Postcode = c.Int(nullable: false),
                        AdressId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Adress", t => t.AdressId, cascadeDelete: true)
                .Index(t => t.AdressId);
            
            CreateTable(
                "dbo.Keys",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PersonalId = c.Long(nullable: false),
                        NumberKey = c.Int(nullable: false),
                        SupplierId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SuppliersAdress", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.Personals",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Surname = c.String(nullable: false, maxLength: 150),
                        Name = c.String(nullable: false, maxLength: 150),
                        MiddleName = c.String(nullable: false, maxLength: 150),
                        Phone1 = c.Int(nullable: false),
                        Phone2 = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 150),
                        SupplierId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SuppliersAdress", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Personals", "SupplierId", "dbo.SuppliersAdress");
            DropForeignKey("dbo.Keys", "SupplierId", "dbo.SuppliersAdress");
            DropForeignKey("dbo.Contracts", "SupplierId", "dbo.SuppliersAdress");
            DropForeignKey("dbo.SuppliersAdress", "StreetId", "dbo.Streets");
            DropForeignKey("dbo.Streets", "AdressId", "dbo.Adress");
            DropIndex("dbo.Personals", new[] { "SupplierId" });
            DropIndex("dbo.Keys", new[] { "SupplierId" });
            DropIndex("dbo.Streets", new[] { "AdressId" });
            DropIndex("dbo.SuppliersAdress", new[] { "StreetId" });
            DropIndex("dbo.Contracts", new[] { "SupplierId" });
            DropTable("dbo.Personals");
            DropTable("dbo.Keys");
            DropTable("dbo.Streets");
            DropTable("dbo.SuppliersAdress");
            DropTable("dbo.Contracts");
            DropTable("dbo.Adress");
        }
    }
}
