namespace Drains.Web.Data.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deliv_parom",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        V = c.Int(nullable: false),
                        Ph = c.Int(nullable: false),
                        T = c.Int(nullable: false),
                        P1 = c.Int(nullable: false),
                        P2 = c.Int(nullable: false),
                        P3 = c.Int(nullable: false),
                        DeliveryId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Deliverys", t => t.DeliveryId, cascadeDelete: true)
                .Index(t => t.DeliveryId);
            
            CreateTable(
                "dbo.Deliverys",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Num_key = c.Int(nullable: false),
                        ContractId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contracts", t => t.ContractId, cascadeDelete: true)
                .Index(t => t.ContractId);
            
            CreateTable(
                "dbo.Paids",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        SumToPaid = c.Int(nullable: false),
                        Paid = c.Int(nullable: false),
                        ContractId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contracts", t => t.ContractId, cascadeDelete: true)
                .Index(t => t.ContractId);
            
            CreateTable(
                "dbo.Permision",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Permission = c.Boolean(nullable: false),
                        ContractId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contracts", t => t.ContractId, cascadeDelete: true)
                .Index(t => t.ContractId);
            
            CreateTable(
                "dbo.SupplierBanks",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MFO = c.String(nullable: false),
                        RR = c.String(nullable: false),
                        Bank = c.String(nullable: false),
                        EDRPOU = c.Long(nullable: false),
                        BankBranch = c.String(nullable: false),
                        REQ = c.String(nullable: false),
                        SupplierId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SuppliersAdress", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierBanks", "SupplierId", "dbo.SuppliersAdress");
            DropForeignKey("dbo.Permision", "ContractId", "dbo.Contracts");
            DropForeignKey("dbo.Paids", "ContractId", "dbo.Contracts");
            DropForeignKey("dbo.Deliv_parom", "DeliveryId", "dbo.Deliverys");
            DropForeignKey("dbo.Deliverys", "ContractId", "dbo.Contracts");
            DropIndex("dbo.SupplierBanks", new[] { "SupplierId" });
            DropIndex("dbo.Permision", new[] { "ContractId" });
            DropIndex("dbo.Paids", new[] { "ContractId" });
            DropIndex("dbo.Deliverys", new[] { "ContractId" });
            DropIndex("dbo.Deliv_parom", new[] { "DeliveryId" });
            DropTable("dbo.SupplierBanks");
            DropTable("dbo.Permision");
            DropTable("dbo.Paids");
            DropTable("dbo.Deliverys");
            DropTable("dbo.Deliv_parom");
        }
    }
}
