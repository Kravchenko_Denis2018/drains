﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("Personals")]
    public class Personals
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        [StringLength(150)]
        public string Surname { get; set; }
        [Required]
        [StringLength(150)]
        public string Name { get; set; }
        [Required]
        [StringLength(150)]
        public string MiddleName { get; set; }
        [Required]
        public int Phone1 { get; set; }
        [Required]
        public int? Phone2 { get; set; }
        [Required]
        [StringLength(150)]
        public string Email { get; set; }
        [ForeignKey("Supplier")]
        public long SupplierId { get; set; }

        public virtual SuppliersAdress Supplier { get; set; }
    }
}
