﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("Streets")]
    public class Streets
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        [StringLength(150)]
        public string Street { get; set; }
        [Required]
        public int Postcode { get; set; }
        [ForeignKey("Adress")]
        public long AdressId { get; set; }

        public virtual Adress Adress { get; set; }
    }
}
