﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("Adress")]
    public class Adress
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        [StringLength(150)]
        public string Region { get; set; }
        [Required]
        [StringLength(150)]
        public string District { get; set; }
        [Required]
        [StringLength(150)]
        public string Location { get; set; }
    }
}
