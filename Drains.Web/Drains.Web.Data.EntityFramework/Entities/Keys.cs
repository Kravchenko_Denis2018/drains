﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("Keys")]
    public class Keys
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long PersonalId { get; set; }
        public int NumberKey { get; set; }
        [ForeignKey("Supplier")]
        public long SupplierId { get; set; }

        public virtual SuppliersAdress Supplier { get; set; }
    }
}
