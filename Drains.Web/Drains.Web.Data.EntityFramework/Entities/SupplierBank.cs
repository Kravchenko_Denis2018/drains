﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Drains.Web.Data.EntityFramework.Entities
{
    public class SupplierBank
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public string MFO { get; set; }
        [Required]
        public string RR { get; set; }
        [Required]
        public string Bank { get; set; }
        [Required]
        public long EDRPOU { get; set; }
        [Required]
        public string BankBranch { get; set; }
        [Required]
        public string REQ { get; set; }
        [ForeignKey("Supplier")]
        public long SupplierId { get; set; }

        public virtual SuppliersAdress Supplier { get; set; }
    }
}
