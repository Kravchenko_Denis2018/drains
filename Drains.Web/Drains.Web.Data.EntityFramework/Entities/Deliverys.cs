﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("Deliverys")]
    public class Deliverys
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public DateTime Time { get; set; }
        [Required]
        public int Num_key { get; set; }
        [ForeignKey("Contract")]
        public long ContractId { get; set; }
        public virtual Contracts Contract { get; set; }
    }
}
