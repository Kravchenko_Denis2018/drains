﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("Deliv_parom")]
    public class Deliv_parom
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public int V { get; set; }
        [Required]
        public int Ph { get; set; }
        [Required]
        public int T { get; set; }
        [Required]
        public int P1 { get; set; }
        [Required]
        public int P2 { get; set; }
        [Required]
        public int P3 { get; set; }
        [ForeignKey("Delivery")]
        public long DeliveryId { get; set; }
        public virtual Deliverys Delivery { get; set; }
    }
}
