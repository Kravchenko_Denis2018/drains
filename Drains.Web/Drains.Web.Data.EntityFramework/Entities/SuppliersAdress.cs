﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("SuppliersAdress")]
    public class SuppliersAdress
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public long SuppliersCode { get; set; }
        [Required]
        public string Build { get; set; }
        [Required]
        public string Apartment { get; set; }
        [Required]
        public int Phone1 { get; set; }
        public int? Phone2 { get; set; }
        public int? Phone3 { get; set; }
        [Required]
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string NameSupplier { get; set; }
        [ForeignKey("Street")]
        public long StreetId { get; set; }

        public virtual Streets Street { get; set; }
    }
}
