﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("Permision")]
    public class Permision
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public bool Permission { get; set; }
        [ForeignKey("Contract")]
        public long ContractId { get; set; }
        public virtual Contracts Contract { get; set; }
    }
}
