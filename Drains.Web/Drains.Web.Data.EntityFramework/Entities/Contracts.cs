﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Drains.Web.Data.EntityFramework.Entities
{
    [Table("Contracts")]
    public class Contracts
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public DateTime DateNow { get; set; }
        [Required]
        public DateTime DateEnd { get; set; }
        [ForeignKey("Supplier")]
        public long SupplierId { get; set; }
        public virtual SuppliersAdress Supplier { get; set; }
    }
}
