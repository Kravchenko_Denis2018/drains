﻿namespace Drains.Web.Data.EntityFramework
{
    using Drains.Web.Data.EntityFramework.Entities;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class DrainsStorageModel : DbContext
    {
        // Контекст настроен для использования строки подключения "DrainsStorageModel" из файла конфигурации  
        // приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
        // "Drains.Web.Data.EntityFramework.DrainsStorageModel" в экземпляре LocalDb. 
        // 
        // Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "DrainsStorageModel" 
        // в файле конфигурации приложения.
        public DrainsStorageModel()
            : base("name=DrainsStorageModel")
        {
           Database.SetInitializer(new CreateDatabaseIfNotExists<DrainsStorageModel>());
           Database.SetInitializer(new DropCreateDatabaseAlways<DrainsStorageModel>());
           Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DrainsStorageModel>());
        }
        public virtual DbSet<Adress> Adresses { get; set; }
        public virtual DbSet<Streets> Streets { get; set; }
        public virtual DbSet<SuppliersAdress> SuppliersAdresses { get;set;}
        public virtual DbSet<Personals> Personals { get; set; }
        public virtual DbSet<Contracts> Contracts { get; set; }
        public virtual DbSet<Keys> Keys { get; set; }
        public virtual DbSet<SupplierBank> SupplierBanks { get; set; }
        public virtual DbSet<Deliverys> Deliverys { get; set; }
        public virtual DbSet<Paids> Paids { get; set; }
        public virtual DbSet<Permision> Permisions { get; set; }
        public virtual DbSet<Deliv_parom> Deliv_Paroms { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Configurations.Add(new ShopActionMap());

        }

        public static DrainsStorageModel Create()
        {
            return new DrainsStorageModel();
        }
        // Добавьте DbSet для каждого типа сущности, который требуется включить в модель. Дополнительные сведения 
        // о настройке и использовании модели Code First см. в статье http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}