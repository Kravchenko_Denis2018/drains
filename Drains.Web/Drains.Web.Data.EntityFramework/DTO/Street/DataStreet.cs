﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Drains.Web.Data.Contracts.DTO.Street;

namespace Drains.Web.Data.EntityFramework.DTO.Street
{
    public class DataStreet : IDataStreet
    {
        public long Id { get; set; }

        public string Street { get; set; }

        public int Postcode { get; set; }

        public long AdressId { get; set; }
    }
}
