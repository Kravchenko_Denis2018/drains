﻿using Drains.Web.Data.Contracts.DTO.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.DTO.Contract
{
    public class DataContract : IDataContract
    {
        public long Id { get; set; }

        public int Number { get; set; }

        public DateTime DateNow { get; set; }

        public DateTime DateEnd { get; set; }

        public long SupplierId { get; set; }
    }
}
