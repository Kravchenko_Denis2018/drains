﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Drains.Web.Data.Contracts.DTO.Adress;

namespace Drains.Web.Data.EntityFramework.DTO.Adress
{
    public class DataAdress : IDataAdress
    {
        public long Id { get; set; }

        public string Region { get; set; }

        public string District { get; set; }

        public string Location { get; set; }
    }
}
