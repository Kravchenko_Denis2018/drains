﻿using Drains.Web.Data.Contracts.DTO.Key;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.EntityFramework.DTO.Key
{
    public class DataKey : IDataKey
    {
        public long Id { get; set; }

        public long PersonalId { get; set; }

        public int NumberKey { get; set; }

        public long SupplierId { get; set; }
    }
}
