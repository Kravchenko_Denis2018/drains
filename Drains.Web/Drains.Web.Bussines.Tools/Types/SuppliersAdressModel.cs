﻿using Drains.Web.Bussines.Contracts.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Types
{
    public class SuppliersAdressModel : ISuppliersAdressModel
    {
        public long Id { get; set; }

        public long SuppliersCode { get; set; }

        public string Build { get; set; }

        public string Apartment { get; set; }

        public int Phone1 { get; set; }

        public int? Phone2 { get; set; }

        public int? Phone3 { get; set; }

        public string Email1 { get; set; }

        public string Email2 { get; set; }

        public string NameSupplier { get; set; }

        public long StreetId { get; set; }
    }
}
