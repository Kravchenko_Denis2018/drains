﻿using Drains.Web.Bussines.Contracts.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Types
{
    public class KeyModel : IKeyModel
    {
        public long Id { get; set; }

        public long PersonalId { get; set; }

        public int NumberKey { get; set; }

        public long SupplierId { get; set; }
    }
}
