﻿using Drains.Web.Bussines.Contracts.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Types
{
    public class ContractModel : IContractModel
    {
        public long Id { get; set; }

        public int Number { get; set; }

        public DateTime DateNow { get; set; }

        public DateTime DateEnd { get; set; }

        public long SupplierId { get; set; }
    }
}
