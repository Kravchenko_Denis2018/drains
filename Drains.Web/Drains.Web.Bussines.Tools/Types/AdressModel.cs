﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Drains.Web.Bussines.Contracts.Types;

namespace Drains.Web.Bussines.Tools.Types
{
    public class AdressModel : IAdressModel
    {
        public long Id { get; set; }

        public string Region { get; set; }

        public string District { get; set; }

        public string Location { get; set; }
    }
}
