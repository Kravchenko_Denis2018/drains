﻿using Drains.Web.Bussines.Contracts.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Types
{
    public class PersonalModel : IPersonalModel
    {
        public long Id { get; set; }

        public string Surname { get; set; }

        public string Name { get; set; }

        public string MiddleName { get; set; }

        public int Phone1 { get; set; }

        public int? Phone2 { get; set; }

        public string Email { get; set; }

        public long SupplierId { get; set; }
    }
}
