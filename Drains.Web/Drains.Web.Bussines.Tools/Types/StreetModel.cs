﻿using Drains.Web.Bussines.Contracts.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Types
{
    public class StreetModel : IStreetModel
    {
        public long Id { get; set; }

        public string Street { get; set; }

        public int Postcode { get; set; }

        public long AdressId { get; set; }
    }
}
