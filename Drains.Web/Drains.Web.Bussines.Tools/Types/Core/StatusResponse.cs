﻿using Drains.Web.Bussines.Contracts.Types.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Types.Core
{
    public class StatusResponse : IStatusResponse
    {
        public long Id { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }

        public StatusResponse()
        {
            IsSuccess = true;
        }

        public StatusResponse(long id)
        {
            IsSuccess = true;
            Id = id;
        }

        public StatusResponse(string code, string message)
        {
            IsSuccess = false;
            ErrorCode = code;
            ErrorMessage = message;
        }

    }
}
