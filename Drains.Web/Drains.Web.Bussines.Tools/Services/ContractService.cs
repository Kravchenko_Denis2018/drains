﻿using Drains.Web.Bussines.Contracts.Services;
using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using Drains.Web.Bussines.Tools.Types;
using Drains.Web.Bussines.Tools.Types.Core;
using Drains.Web.Data.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Services
{
    public class ContractService : IContractService
    {
        private readonly IContractRepository _contractRepository;
        public ContractService(IContractRepository contractRepository)
        {
            _contractRepository = contractRepository;
        }

        public IStatusResponse Create(int number, DateTime dateNow, DateTime dateEnd, long supplierId)
        {
            var id = _contractRepository.Create(number, dateNow, dateEnd, supplierId);
            return new StatusResponse(id);
        }

        public IStatusResponse Delete(long id)
        {
            _contractRepository.Delete(id);
            return new StatusResponse();
        }

        IContractModel IContractService.GetAdressById(long id)
        {
            var model = _contractRepository.GetContractById(id);
            return new ContractModel
            {
                Id = model.Id,
                Number = model.Number,
                DateNow = model.DateNow,
                DateEnd = model.DateEnd,
                SupplierId = model.SupplierId
            };
        }

        public IEnumerable<IContractModel> GetAll()
        {
            var model = _contractRepository.GetAll();
            return model.Select(m => new ContractModel()
            {
                Id = m.Id,
                Number = m.Number,
                DateNow = m.DateNow,
                DateEnd = m.DateEnd,
                SupplierId = m.SupplierId

            });
        }

        public IStatusResponse Update(long id, int number, DateTime dateNow, DateTime dateEnd, long supplierId)
        {
            _contractRepository.Update(id, number, dateNow, dateEnd, supplierId);
            return new StatusResponse();
        }
    }
}
