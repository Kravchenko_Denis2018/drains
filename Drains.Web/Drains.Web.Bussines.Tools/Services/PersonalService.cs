﻿using Drains.Web.Bussines.Contracts.Services;
using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using Drains.Web.Bussines.Tools.Types;
using Drains.Web.Bussines.Tools.Types.Core;
using Drains.Web.Data.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Services
{
    public class PersonalService : IPersonalService
    {
        private readonly IPersonalRepository _personalRepository;
        public PersonalService(IPersonalRepository personalRepository)
        {
            _personalRepository = personalRepository;
        }

        public IStatusResponse Create(string surname, string name, string middleName, int phone1, int? phone2, string email, long supplierId)
        {
            var id = _personalRepository.Create(surname, name, middleName, phone1, phone2, email, supplierId);
            return new StatusResponse(id);
        }

        public IStatusResponse Delete(long id)
        {
            _personalRepository.Delete(id);
            return new StatusResponse();
        }

        IPersonalModel IPersonalService.GetAdressById(long id)
        {
            var model = _personalRepository.GetPersonalById(id);
            return new PersonalModel
            {
                Id = model.Id,
                Surname = model.Surname,
                Name = model.Name,
                MiddleName = model.MiddleName,
                Phone1 = model.Phone1,
                Phone2 = model.Phone2,
                Email = model.Email,
                SupplierId = model.SupplierId
            };
                        
        }

        public IEnumerable<IPersonalModel> GetAll()
        {
            var model = _personalRepository.GetAll();
            return model.Select(m => new PersonalModel()
            {
                Id = m.Id,
                Surname = m.Surname,
                Name = m.Name,
                MiddleName = m.MiddleName,
                Phone1 = m.Phone1,
                Phone2 = m.Phone2,
                Email = m.Email,
                SupplierId = m.SupplierId

            });
        }

        public IStatusResponse Update(long id, string surname, string name, string middleName, int phone1, int? phone2, string email, long supplierId)
        {
            _personalRepository.Update(id, surname, name, middleName, phone1, phone2, email, supplierId);
            return new StatusResponse();
        }
    }
}
