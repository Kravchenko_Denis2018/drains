﻿using Drains.Web.Bussines.Contracts.Services;
using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using Drains.Web.Bussines.Tools.Types;
using Drains.Web.Bussines.Tools.Types.Core;
using Drains.Web.Data.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Services
{
    public class KeyService : IKeyService
    {
        private readonly IKeyRepository _keyRepository;
        public KeyService(IKeyRepository keyRepository)
        {
            _keyRepository = keyRepository;
        }

        public IStatusResponse Create(long personalId, int numberKey, long supplierId)
        {
            var id = _keyRepository.Create(personalId, numberKey, supplierId);
            return new StatusResponse(id);
        }

        public IStatusResponse Delete(long id)
        {
            _keyRepository.Delete(id);
            return new StatusResponse();
        }

        public IEnumerable<IKeyModel> GetAll()
        {
            var model = _keyRepository.GetAll();
            return model.Select(m => new KeyModel()
            {
                Id = m.Id,
                PersonalId = m.PersonalId,
                NumberKey = m.NumberKey,
                SupplierId = m.SupplierId

            });
        }

        IKeyModel IKeyService.GetKeyById(long id)
        {
            var model = _keyRepository.GetKeyById(id);
            return new KeyModel
            {
                Id = model.Id,
                PersonalId = model.PersonalId,
                NumberKey = model.NumberKey,
                SupplierId = model.SupplierId
            };
        }

        public IStatusResponse Update(long id, long personalId, int numberKey, long supplierId)
        {
            _keyRepository.Update(id, personalId, numberKey, supplierId);
            return new StatusResponse();
        }
    }
}
