﻿using Drains.Web.Bussines.Contracts.Services;
using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using Drains.Web.Bussines.Tools.Types;
using Drains.Web.Bussines.Tools.Types.Core;
using Drains.Web.Data.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Services
{
    public class StreetService : IStreetService
    {
        private readonly IStreetRepository _streetRepository;
        public StreetService(IStreetRepository streetRepository)
        {
            _streetRepository = streetRepository;
        }

        public IStatusResponse Create(string street, int postcode, int adressId)
        {
            var id = _streetRepository.Create(street, postcode, adressId);
            return new StatusResponse(id);
        }

        public IStatusResponse Delete(long id)
        {
            _streetRepository.Delete(id);
            return new StatusResponse();
        }

        public IEnumerable<IStreetModel> GetAll()
        {
            var model = _streetRepository.GetAll();
            return model.Select(m => new StreetModel()
            {
                Id = m.Id,
                Street = m.Street,
                Postcode = m.Postcode,
                AdressId = m.AdressId

            });
        }

        IStreetModel IStreetService.GetStreetById(long id)
        {
            var model = _streetRepository.GetStreetById(id);
            return new StreetModel
            {
                Id = model.Id,
                Street = model.Street,
                Postcode = model.Postcode,
                AdressId = model.AdressId
            };
        }

        public IStatusResponse Update(long id, string street, int postcode, int adressId)
        {
            _streetRepository.Update(id, street, postcode, adressId);
            return new StatusResponse();
        }
    }
}
