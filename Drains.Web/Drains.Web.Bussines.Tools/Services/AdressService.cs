﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Drains.Web.Bussines.Contracts.Services;
using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using Drains.Web.Bussines.Tools.Types;
using Drains.Web.Bussines.Tools.Types.Core;
using Drains.Web.Data.Contracts.Repositories;

namespace Drains.Web.Bussines.Tools.Services
{
    public class AdressService : IAdressService
    {
        private readonly IAdressRepository _adressRepository;
        public AdressService(IAdressRepository adressService)
        {
            _adressRepository = adressService;
        }
        public IStatusResponse Create(string region, string district, string location)
        {
            var id = _adressRepository.Create(region, district,location);
            return new StatusResponse(id);
        }

        public IStatusResponse Delete(long id)
        {
            _adressRepository.Delete(id);
            return new StatusResponse();
        }

        IAdressModel IAdressService.GetAdressById(long id)
        {
            var model = _adressRepository.GetAdressById(id);
            return new AdressModel
            {
                Id = model.Id,
                District = model.District,
                Location = model.Location,
                Region = model.Region
            };
        }

        public IEnumerable<IAdressModel> GetAll()
        {
            var model = _adressRepository.GetAll();
            return model.Select(m => new AdressModel()
            {
                Id = m.Id,
                District = m.District,
                Region = m.Region,
                Location = m.Location
                
            });
        }

        public IStatusResponse Update(long id, string region, string district, string location)
        {
            _adressRepository.Update(id, region, district, location);
            return new StatusResponse();
        }
    }
}
