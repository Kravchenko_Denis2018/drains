﻿using Drains.Web.Bussines.Contracts.Services;
using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using Drains.Web.Bussines.Tools.Types;
using Drains.Web.Bussines.Tools.Types.Core;
using Drains.Web.Data.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Tools.Services
{
    public class SuppliersAdressService : ISuppliersAdressService
    {
        ISuppliersAdressRepository _suppliersAdressRepository;
        public SuppliersAdressService(ISuppliersAdressRepository suppliersAdressRepository)
        {
            _suppliersAdressRepository = suppliersAdressRepository;
        }

        public IStatusResponse Create(long suppliersCode, string build, string apartment, int phone1, int? phone2, int? phone3, string email1, string email2, string nameSupplier, long streetId)
        {
            var id = _suppliersAdressRepository.Create(suppliersCode, build, apartment, phone1, phone2, phone3, email1, email2, nameSupplier, streetId);
            return new StatusResponse(id);
        }

        public IStatusResponse Delete(long id)
        {
            _suppliersAdressRepository.Delete(id);
            return new StatusResponse();
        }

        ISuppliersAdressModel ISuppliersAdressService.GetAdressById(long id)
        {
            var model = _suppliersAdressRepository.GetSuppliersAdressById(id);
            return new SuppliersAdressModel
            {
                Id = model.Id,
                SuppliersCode = model.SuppliersCode,
                Build = model.Build,
                Apartment = model.Apartment,
                Phone1 = model.Phone1,
                Phone2 = model.Phone2,
                Phone3 = model.Phone3,
                Email1 = model.Email1,
                Email2 = model.Email2,
                NameSupplier = model.NameSupplier,
                StreetId = model.StreetId
            };
        }

        public IEnumerable<ISuppliersAdressModel> GetAll()
        {
            var model = _suppliersAdressRepository.GetAll();
            return model.Select(m => new SuppliersAdressModel()
            {
                Id = m.Id,
                SuppliersCode = m.SuppliersCode,
                Build = m.Build,
                Apartment = m.Apartment,
                Phone1 = m.Phone1,
                Phone2 = m.Phone2,
                Phone3 = m.Phone3,
                Email1 = m.Email1,
                Email2 = m.Email2,
                NameSupplier = m.NameSupplier,
                StreetId = m.StreetId

            });
        }

        public IStatusResponse Update(long id, long suppliersCode, string build, string apartment, int phone1, int? phone2, int? phone3, string email1, string email2, string nameSupplier, long streetId)
        {
            _suppliersAdressRepository.Update(id, suppliersCode, build, apartment, phone1,  phone2, phone3, email1, email2, nameSupplier, streetId);
            return new StatusResponse();
        }
    }
}
