﻿using Drains.Web.Bussines.Contracts.Services;
using Drains.Web.Bussines.Tools.Services;
using Ninject.Modules;

namespace Drains.Web.Manager.IoC
{
    public class BusinessDependencyResolver : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IAdressService>().To<AdressService>();
            this.Bind<IStreetService>().To<StreetService>();
            this.Bind<ISuppliersAdressService>().To<SuppliersAdressService>();
            this.Bind<IKeyService>().To<KeyService>();

        }
    }
}
