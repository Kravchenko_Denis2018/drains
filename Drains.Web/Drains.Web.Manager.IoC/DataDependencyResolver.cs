﻿using Drains.Web.Data.Contracts.Repositories;
using Drains.Web.Data.EntityFramework.Repositories;
using Ninject.Modules;


namespace Drains.Web.Manager.IoC
{
    public class DataDependencyResolver : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IAdressRepository>().To<AdressRepository>();
            this.Bind<IStreetRepository>().To<StreetRepository>();
            this.Bind<ISuppliersAdressRepository>().To<SuppliersAdressRepository>();
            this.Bind<IKeyRepository>().To<KeyRepository>();
        }
    }
}
