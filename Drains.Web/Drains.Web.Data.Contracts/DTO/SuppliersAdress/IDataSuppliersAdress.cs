﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.DTO.SuppliersAdress
{
    public interface IDataSuppliersAdress
    {
        long Id { get; }
        long SuppliersCode { get; }
        string Build { get; }
        string Apartment { get; }
        int Phone1 { get; }
        int? Phone2 { get; }
        int? Phone3 { get; }
        string Email1 { get; }
        string Email2 { get; }
        string NameSupplier { get; }
        long StreetId { get; }
    }
}
