﻿
namespace Drains.Web.Data.Contracts.DTO.Adress
{
    public interface IDataAdress
    {
        long Id { get; }
        string Region { get; }
        string District { get; }
        string Location { get; }
    }
}
