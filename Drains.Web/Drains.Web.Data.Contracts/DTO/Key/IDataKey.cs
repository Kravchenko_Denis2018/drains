﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.DTO.Key
{
    public interface IDataKey
    {
        long Id { get; }
        long PersonalId { get; }
        int NumberKey { get; }
        long SupplierId { get; }
    }
}
