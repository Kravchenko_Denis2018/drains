﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.DTO.Street
{
    public interface IDataStreet
    {
        long Id { get; }
        string Street { get; }
        int Postcode { get; }
        long AdressId { get; }
    }
}
