﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.DTO.Contract
{
    public interface IDataContract
    {
        long Id { get; }
        int Number { get; }
        DateTime DateNow { get; }
        DateTime DateEnd { get; }
        long SupplierId { get; }
    }
}
