﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.DTO.Personal
{
    public interface IDataPersonal
    {
        long Id { get; }
        string Surname { get; }
        string Name { get; }
        string MiddleName { get; }
        int Phone1 { get; }
        int? Phone2 { get; }
        string Email { get; }
        long SupplierId { get; }

    }
}
