﻿using Drains.Web.Data.Contracts.DTO.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.Repositories
{
    public interface IContractRepository
    {
        IDataContract GetContractById(long id);
        IEnumerable<IDataContract> GetAll();
        long Create(int number, DateTime dateNow, DateTime dateEnd, long supplierId);
        bool Delete(long id);
        bool Update(long id, int number, DateTime dateNow, DateTime dateEnd, long supplierId);
    }
}
