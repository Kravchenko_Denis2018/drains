﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Drains.Web.Data.Contracts.DTO.Street;

namespace Drains.Web.Data.Contracts.Repositories
{
    public interface IStreetRepository
    {
        IDataStreet GetStreetById(long id);
        IEnumerable<IDataStreet> GetAll();
        long Create(string street, int postcode, int adressId);
        bool Delete(long id);
        bool Update(long id, string street, int postcode, int adressId);
    }
}
