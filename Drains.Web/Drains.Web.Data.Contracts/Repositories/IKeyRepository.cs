﻿using Drains.Web.Data.Contracts.DTO.Key;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.Repositories
{
    public interface IKeyRepository
    {
        IDataKey GetKeyById(long id);
        IEnumerable<IDataKey> GetAll();
        long Create(long personalId, int numberKey, long supplierId);
        bool Delete(long id);
        bool Update(long id, long personalId, int numberKey, long supplierId);
    }
}
