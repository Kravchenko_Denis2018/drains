﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Drains.Web.Data.Contracts.DTO.Adress;

namespace Drains.Web.Data.Contracts.Repositories
{
    public interface IAdressRepository
    {
        IDataAdress GetAdressById(long id);
        IEnumerable<IDataAdress> GetAll();
        long Create(string region, string district, string location);
        bool Delete(long id);
        bool Update(long id, string region, string district, string location);
    }
}
