﻿using Drains.Web.Data.Contracts.DTO.Personal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.Repositories
{
    public interface IPersonalRepository
    {
        IDataPersonal GetPersonalById(long id);
        IEnumerable<IDataPersonal> GetAll();
        long Create(string surname, string name, string middleName, int phone1, int? phone2, string email, long supplierId);
        bool Delete(long id);
        bool Update(long id, string surname, string name, string middleName, int phone1, int? phone2, string email, long supplierId);
    }
}
