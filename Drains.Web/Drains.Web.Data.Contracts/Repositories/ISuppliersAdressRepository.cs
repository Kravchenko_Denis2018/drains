﻿using Drains.Web.Data.Contracts.DTO.SuppliersAdress;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Data.Contracts.Repositories
{
    public interface ISuppliersAdressRepository
    {
        IDataSuppliersAdress GetSuppliersAdressById(long id);
        IEnumerable<IDataSuppliersAdress> GetAll();
        long Create(long suppliersCode, string build, string apartment, int phone1, int? phone2, int? phone3, string email1, string email2, string nameSupplier, long streetId);
        bool Delete(long id);
        bool Update(long id, long suppliersCode, string build, string apartment, int phone1, int? phone2, int? phone3, string email1, string email2, string nameSupplier, long streetId);
    }
}
