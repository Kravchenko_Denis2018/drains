﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Drains.Web.Data.EntityFramework;
using Drains.Web.Data.EntityFramework.Entities;

namespace Drains.Web.Controllers
{
    public class DeliverysController : Controller
    {
        private DrainsStorageModel db = new DrainsStorageModel();

        // GET: Deliverys
        public ActionResult Index()
        {
            var deliverys = db.Deliverys.Include(d => d.Contract);
            return View(deliverys.ToList());
        }

        // GET: Deliverys/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Deliverys deliverys = db.Deliverys.Find(id);
            if (deliverys == null)
            {
                return HttpNotFound();
            }
            return View(deliverys);
        }

        // GET: Deliverys/Create
        public ActionResult Create()
        {
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id");
            return View();
        }

        // POST: Deliverys/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,Time,Num_key,ContractId")] Deliverys deliverys)
        {
            if (ModelState.IsValid)
            {
                db.Deliverys.Add(deliverys);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", deliverys.ContractId);
            return View(deliverys);
        }

        // GET: Deliverys/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Deliverys deliverys = db.Deliverys.Find(id);
            if (deliverys == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", deliverys.ContractId);
            return View(deliverys);
        }

        // POST: Deliverys/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,Time,Num_key,ContractId")] Deliverys deliverys)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deliverys).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", deliverys.ContractId);
            return View(deliverys);
        }

        // GET: Deliverys/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Deliverys deliverys = db.Deliverys.Find(id);
            if (deliverys == null)
            {
                return HttpNotFound();
            }
            return View(deliverys);
        }

        // POST: Deliverys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Deliverys deliverys = db.Deliverys.Find(id);
            db.Deliverys.Remove(deliverys);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
