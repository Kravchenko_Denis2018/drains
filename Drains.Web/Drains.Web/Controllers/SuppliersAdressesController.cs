﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Drains.Web.Data.EntityFramework;
using Drains.Web.Data.EntityFramework.Entities;

namespace Drains.Web.Controllers
{
    public class SuppliersAdressesController : Controller
    {
        private DrainsStorageModel db = new DrainsStorageModel();

        // GET: SuppliersAdresses
        public ActionResult Index()
        {
            var suppliersAdresses = db.SuppliersAdresses.Include(s => s.Street);
            return View(suppliersAdresses.ToList());
        }

        // GET: SuppliersAdresses/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuppliersAdress suppliersAdress = db.SuppliersAdresses.Find(id);
            if (suppliersAdress == null)
            {
                return HttpNotFound();
            }
            return View(suppliersAdress);
        }

        // GET: SuppliersAdresses/Create
        public ActionResult Create()
        {
            ViewBag.StreetId = new SelectList(db.Streets, "Id", "Street");
            return View();
        }

        // POST: SuppliersAdresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SuppliersCode,Build,Apartment,Phone1,Phone2,Phone3,Email1,Email2,NameSupplier,StreetId")] SuppliersAdress suppliersAdress)
        {
            if (ModelState.IsValid)
            {
                db.SuppliersAdresses.Add(suppliersAdress);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StreetId = new SelectList(db.Streets, "Id", "Street", suppliersAdress.StreetId);
            return View(suppliersAdress);
        }

        // GET: SuppliersAdresses/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuppliersAdress suppliersAdress = db.SuppliersAdresses.Find(id);
            if (suppliersAdress == null)
            {
                return HttpNotFound();
            }
            ViewBag.StreetId = new SelectList(db.Streets, "Id", "Street", suppliersAdress.StreetId);
            return View(suppliersAdress);
        }

        // POST: SuppliersAdresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SuppliersCode,Build,Apartment,Phone1,Phone2,Phone3,Email1,Email2,NameSupplier,StreetId")] SuppliersAdress suppliersAdress)
        {
            if (ModelState.IsValid)
            {
                db.Entry(suppliersAdress).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StreetId = new SelectList(db.Streets, "Id", "Street", suppliersAdress.StreetId);
            return View(suppliersAdress);
        }

        // GET: SuppliersAdresses/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SuppliersAdress suppliersAdress = db.SuppliersAdresses.Find(id);
            if (suppliersAdress == null)
            {
                return HttpNotFound();
            }
            return View(suppliersAdress);
        }

        // POST: SuppliersAdresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            SuppliersAdress suppliersAdress = db.SuppliersAdresses.Find(id);
            db.SuppliersAdresses.Remove(suppliersAdress);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
