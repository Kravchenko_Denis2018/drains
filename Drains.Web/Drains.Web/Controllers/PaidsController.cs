﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Drains.Web.Data.EntityFramework;
using Drains.Web.Data.EntityFramework.Entities;

namespace Drains.Web.Controllers
{
    public class PaidsController : Controller
    {
        private DrainsStorageModel db = new DrainsStorageModel();

        // GET: Paids
        public ActionResult Index()
        {
            var paids = db.Paids.Include(p => p.Contract);
            return View(paids.ToList());
        }

        // GET: Paids/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paids paids = db.Paids.Find(id);
            if (paids == null)
            {
                return HttpNotFound();
            }
            return View(paids);
        }

        // GET: Paids/Create
        public ActionResult Create()
        {
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id");
            return View();
        }

        // POST: Paids/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,SumToPaid,Paid,ContractId")] Paids paids)
        {
            if (ModelState.IsValid)
            {
                db.Paids.Add(paids);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", paids.ContractId);
            return View(paids);
        }

        // GET: Paids/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paids paids = db.Paids.Find(id);
            if (paids == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", paids.ContractId);
            return View(paids);
        }

        // POST: Paids/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,SumToPaid,Paid,ContractId")] Paids paids)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paids).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", paids.ContractId);
            return View(paids);
        }

        // GET: Paids/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paids paids = db.Paids.Find(id);
            if (paids == null)
            {
                return HttpNotFound();
            }
            return View(paids);
        }

        // POST: Paids/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Paids paids = db.Paids.Find(id);
            db.Paids.Remove(paids);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
