﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Drains.Web.Data.EntityFramework;
using Drains.Web.Data.EntityFramework.Entities;

namespace Drains.Web.Controllers
{
    public class Deliv_paromController : Controller
    {
        private DrainsStorageModel db = new DrainsStorageModel();

        // GET: Deliv_parom
        public ActionResult Index()
        {
            var deliv_Paroms = db.Deliv_Paroms.Include(d => d.Delivery);
            return View(deliv_Paroms.ToList());
        }

        // GET: Deliv_parom/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Deliv_parom deliv_parom = db.Deliv_Paroms.Find(id);
            if (deliv_parom == null)
            {
                return HttpNotFound();
            }
            return View(deliv_parom);
        }

        // GET: Deliv_parom/Create
        public ActionResult Create()
        {
            ViewBag.DeliveryId = new SelectList(db.Deliverys, "Id", "Id");
            return View();
        }

        // POST: Deliv_parom/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,V,Ph,T,P1,P2,P3,DeliveryId")] Deliv_parom deliv_parom)
        {
            if (ModelState.IsValid)
            {
                db.Deliv_Paroms.Add(deliv_parom);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DeliveryId = new SelectList(db.Deliverys, "Id", "Id", deliv_parom.DeliveryId);
            return View(deliv_parom);
        }

        // GET: Deliv_parom/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Deliv_parom deliv_parom = db.Deliv_Paroms.Find(id);
            if (deliv_parom == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeliveryId = new SelectList(db.Deliverys, "Id", "Id", deliv_parom.DeliveryId);
            return View(deliv_parom);
        }

        // POST: Deliv_parom/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,V,Ph,T,P1,P2,P3,DeliveryId")] Deliv_parom deliv_parom)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deliv_parom).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DeliveryId = new SelectList(db.Deliverys, "Id", "Id", deliv_parom.DeliveryId);
            return View(deliv_parom);
        }

        // GET: Deliv_parom/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Deliv_parom deliv_parom = db.Deliv_Paroms.Find(id);
            if (deliv_parom == null)
            {
                return HttpNotFound();
            }
            return View(deliv_parom);
        }

        // POST: Deliv_parom/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Deliv_parom deliv_parom = db.Deliv_Paroms.Find(id);
            db.Deliv_Paroms.Remove(deliv_parom);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
