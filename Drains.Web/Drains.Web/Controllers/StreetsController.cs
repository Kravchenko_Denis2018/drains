﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Drains.Web.Data.EntityFramework;
using Drains.Web.Data.EntityFramework.Entities;

namespace Drains.Web.Controllers
{
    public class StreetsController : Controller
    {
        private DrainsStorageModel db = new DrainsStorageModel();

        // GET: Streets
        public ActionResult Index()
        {
            var streets = db.Streets.Include(s => s.Adress);
            return View(streets.ToList());
        }

        // GET: Streets/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Streets streets = db.Streets.Find(id);
            if (streets == null)
            {
                return HttpNotFound();
            }
            return View(streets);
        }

        // GET: Streets/Create
        public ActionResult Create()
        {
            ViewBag.AdressId = new SelectList(db.Adresses, "Id", "Region");
            return View();
        }

        // POST: Streets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Street,Postcode,AdressId")] Streets streets)
        {
            if (ModelState.IsValid)
            {
                db.Streets.Add(streets);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AdressId = new SelectList(db.Adresses, "Id", "Region", streets.AdressId);
            return View(streets);
        }

        // GET: Streets/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Streets streets = db.Streets.Find(id);
            if (streets == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdressId = new SelectList(db.Adresses, "Id", "Region", streets.AdressId);
            return View(streets);
        }

        // POST: Streets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Street,Postcode,AdressId")] Streets streets)
        {
            if (ModelState.IsValid)
            {
                db.Entry(streets).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdressId = new SelectList(db.Adresses, "Id", "Region", streets.AdressId);
            return View(streets);
        }

        // GET: Streets/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Streets streets = db.Streets.Find(id);
            if (streets == null)
            {
                return HttpNotFound();
            }
            return View(streets);
        }

        // POST: Streets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Streets streets = db.Streets.Find(id);
            db.Streets.Remove(streets);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
