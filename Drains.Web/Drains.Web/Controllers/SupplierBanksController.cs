﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Drains.Web.Data.EntityFramework;
using Drains.Web.Data.EntityFramework.Entities;

namespace Drains.Web.Controllers
{
    public class SupplierBanksController : Controller
    {
        private DrainsStorageModel db = new DrainsStorageModel();

        // GET: SupplierBanks
        public ActionResult Index()
        {
            var supplierBanks = db.SupplierBanks.Include(s => s.Supplier);
            return View(supplierBanks.ToList());
        }

        // GET: SupplierBanks/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SupplierBank supplierBank = db.SupplierBanks.Find(id);
            if (supplierBank == null)
            {
                return HttpNotFound();
            }
            return View(supplierBank);
        }

        // GET: SupplierBanks/Create
        public ActionResult Create()
        {
            ViewBag.SupplierId = new SelectList(db.SuppliersAdresses, "Id", "Build");
            return View();
        }

        // POST: SupplierBanks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MFO,RR,Bank,EDRPOU,BankBranch,REQ,SupplierId")] SupplierBank supplierBank)
        {
            if (ModelState.IsValid)
            {
                db.SupplierBanks.Add(supplierBank);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SupplierId = new SelectList(db.SuppliersAdresses, "Id", "Build", supplierBank.SupplierId);
            return View(supplierBank);
        }

        // GET: SupplierBanks/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SupplierBank supplierBank = db.SupplierBanks.Find(id);
            if (supplierBank == null)
            {
                return HttpNotFound();
            }
            ViewBag.SupplierId = new SelectList(db.SuppliersAdresses, "Id", "Build", supplierBank.SupplierId);
            return View(supplierBank);
        }

        // POST: SupplierBanks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MFO,RR,Bank,EDRPOU,BankBranch,REQ,SupplierId")] SupplierBank supplierBank)
        {
            if (ModelState.IsValid)
            {
                db.Entry(supplierBank).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SupplierId = new SelectList(db.SuppliersAdresses, "Id", "Build", supplierBank.SupplierId);
            return View(supplierBank);
        }

        // GET: SupplierBanks/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SupplierBank supplierBank = db.SupplierBanks.Find(id);
            if (supplierBank == null)
            {
                return HttpNotFound();
            }
            return View(supplierBank);
        }

        // POST: SupplierBanks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            SupplierBank supplierBank = db.SupplierBanks.Find(id);
            db.SupplierBanks.Remove(supplierBank);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
