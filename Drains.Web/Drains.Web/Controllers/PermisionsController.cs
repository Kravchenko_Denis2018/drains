﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Drains.Web.Data.EntityFramework;
using Drains.Web.Data.EntityFramework.Entities;

namespace Drains.Web.Controllers
{
    public class PermisionsController : Controller
    {
        private DrainsStorageModel db = new DrainsStorageModel();

        // GET: Permisions
        public ActionResult Index()
        {
            var permisions = db.Permisions.Include(p => p.Contract);
            return View(permisions.ToList());
        }

        // GET: Permisions/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Permision permision = db.Permisions.Find(id);
            if (permision == null)
            {
                return HttpNotFound();
            }
            return View(permision);
        }

        // GET: Permisions/Create
        public ActionResult Create()
        {
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id");
            return View();
        }

        // POST: Permisions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Permission,ContractId")] Permision permision)
        {
            if (ModelState.IsValid)
            {
                db.Permisions.Add(permision);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", permision.ContractId);
            return View(permision);
        }

        // GET: Permisions/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Permision permision = db.Permisions.Find(id);
            if (permision == null)
            {
                return HttpNotFound();
            }
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", permision.ContractId);
            return View(permision);
        }

        // POST: Permisions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Permission,ContractId")] Permision permision)
        {
            if (ModelState.IsValid)
            {
                db.Entry(permision).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ContractId = new SelectList(db.Contracts, "Id", "Id", permision.ContractId);
            return View(permision);
        }

        // GET: Permisions/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Permision permision = db.Permisions.Find(id);
            if (permision == null)
            {
                return HttpNotFound();
            }
            return View(permision);
        }

        // POST: Permisions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Permision permision = db.Permisions.Find(id);
            db.Permisions.Remove(permision);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
