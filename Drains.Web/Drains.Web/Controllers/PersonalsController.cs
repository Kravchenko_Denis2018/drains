﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Drains.Web.Data.EntityFramework;
using Drains.Web.Data.EntityFramework.Entities;

namespace Drains.Web.Controllers
{
    public class PersonalsController : Controller
    {
        private DrainsStorageModel db = new DrainsStorageModel();

        // GET: Personals
        public ActionResult Index()
        {
            var personals = db.Personals.Include(p => p.Supplier);
            return View(personals.ToList());
        }

        // GET: Personals/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personals personals = db.Personals.Find(id);
            if (personals == null)
            {
                return HttpNotFound();
            }
            return View(personals);
        }

        // GET: Personals/Create
        public ActionResult Create()
        {
            ViewBag.SupplierId = new SelectList(db.SuppliersAdresses, "Id", "Build");
            return View();
        }

        // POST: Personals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Surname,Name,MiddleName,Phone1,Phone2,Email,SupplierId")] Personals personals)
        {
            if (ModelState.IsValid)
            {
                db.Personals.Add(personals);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SupplierId = new SelectList(db.SuppliersAdresses, "Id", "Build", personals.SupplierId);
            return View(personals);
        }

        // GET: Personals/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personals personals = db.Personals.Find(id);
            if (personals == null)
            {
                return HttpNotFound();
            }
            ViewBag.SupplierId = new SelectList(db.SuppliersAdresses, "Id", "Build", personals.SupplierId);
            return View(personals);
        }

        // POST: Personals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Surname,Name,MiddleName,Phone1,Phone2,Email,SupplierId")] Personals personals)
        {
            if (ModelState.IsValid)
            {
                db.Entry(personals).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SupplierId = new SelectList(db.SuppliersAdresses, "Id", "Build", personals.SupplierId);
            return View(personals);
        }

        // GET: Personals/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personals personals = db.Personals.Find(id);
            if (personals == null)
            {
                return HttpNotFound();
            }
            return View(personals);
        }

        // POST: Personals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Personals personals = db.Personals.Find(id);
            db.Personals.Remove(personals);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
