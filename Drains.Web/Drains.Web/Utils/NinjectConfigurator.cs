﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Ninject;
using Drains.Web.Manager.IoC;

namespace Drains.Web.Utils
{
    public class NinjectConfigurator
    {
        public void Configure(IKernel container)
        {
            // Add all bindings/dependencies

            AddBindings(container);

            // Use the container and our NinjectDependencyResolver as
            // application's resolver
            var resolver = new NinjectDependencyResolver(container);

            System.Web.Mvc.DependencyResolver.SetResolver(resolver); // MVC

            GlobalConfiguration.Configuration.DependencyResolver = resolver; // Web API
        }

        // Omitted for brevity.
        private void AddBindings(IKernel kernel)
        {
            kernel.Load(new BusinessDependencyResolver(), new DataDependencyResolver());
        }
    }
}