﻿using Drains.Web.Bussines.Contracts.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Drains.Web.Models
{
    public class AdressModel : IAdressModel
    {
        public long Id { get; set; }
        public string Region { get; set; }

        public string District { get; set; }

        public string Location { get; set; }
    }
}