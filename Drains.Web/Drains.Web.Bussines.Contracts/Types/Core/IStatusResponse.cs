﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Types.Core
{
    public interface IStatusResponse : ISuccessResponse
    {
        string ErrorCode { get; }
        string ErrorMessage { get; }
    }
}
