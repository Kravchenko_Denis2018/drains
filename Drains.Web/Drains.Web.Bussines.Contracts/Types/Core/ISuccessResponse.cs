﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Types.Core
{
    public interface ISuccessResponse
    {
        long Id { get; }
        bool IsSuccess { get; }
    }
}
