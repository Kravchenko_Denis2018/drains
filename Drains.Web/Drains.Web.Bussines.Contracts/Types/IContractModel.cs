﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Types
{
    public interface IContractModel
    {
        long Id { get; }
        int Number { get; }
        DateTime DateNow { get; }
        DateTime DateEnd { get; }
        long SupplierId { get; }
    }
}
