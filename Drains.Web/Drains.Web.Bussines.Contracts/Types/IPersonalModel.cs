﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Types
{
    public interface IPersonalModel
    {
        long Id { get; }
        string Surname { get; }
        string Name { get; }
        string MiddleName { get; }
        int Phone1 { get; }
        int? Phone2 { get; }
        string Email { get; }
        long SupplierId { get; }
    }
}
