﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Types
{
    public interface IAdressModel
    {
        long Id { get; }
        string Region { get; }
        string District { get; }
        string Location { get; }
    }
}
