﻿using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Drains.Web.Bussines.Contracts.Services
{
    public interface ISuppliersAdressService
    {
        ISuppliersAdressModel GetAdressById(long id);
        IEnumerable<ISuppliersAdressModel> GetAll();
        IStatusResponse Create(long suppliersCode, string build, string apartment, int phone1, int? phone2, int? phone3, string email1, string email2, string nameSupplier, long streetId);
        IStatusResponse Delete(long id);
        IStatusResponse Update(long id, long suppliersCode, string build, string apartment, int phone1, int? phone2, int? phone3, string email1, string email2, string nameSupplier, long streetId);
    }
}