﻿using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Services
{
    public interface IKeyService
    {
        IKeyModel GetKeyById(long id);
        IEnumerable<IKeyModel> GetAll();
        IStatusResponse Create(long personalId, int numberKey, long supplierId);
        IStatusResponse Delete(long id);
        IStatusResponse Update(long id, long personalId, int numberKey, long supplierId);
    }
}
