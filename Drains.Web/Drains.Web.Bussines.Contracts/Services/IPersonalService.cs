﻿using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Services
{
    public interface IPersonalService
    {
        IPersonalModel GetAdressById(long id);
        IEnumerable<IPersonalModel> GetAll();
        IStatusResponse Create(string surname, string name, string middleName, int phone1, int? phone2, string email, long supplierId);
        IStatusResponse Delete(long id);
        IStatusResponse Update(long id, string surname, string name, string middleName, int phone1, int? phone2, string email, long supplierId);
    }
}
