﻿using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Services
{
    public interface IAdressService
    {
        IAdressModel GetAdressById(long id);
        IEnumerable<IAdressModel> GetAll();
        IStatusResponse Create(string region, string district, string location);
        IStatusResponse Delete(long id);
        IStatusResponse Update(long id, string region, string district, string location);
    }
}
