﻿using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Services
{
    public interface IStreetService
    {
        IStreetModel GetStreetById(long id);
        IEnumerable<IStreetModel> GetAll();
        IStatusResponse Create(string street, int postcode, int adressId);
        IStatusResponse Delete(long id);
        IStatusResponse Update(long id, string street, int postcode, int adressId);
    }
}
