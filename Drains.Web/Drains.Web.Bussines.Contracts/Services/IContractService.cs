﻿using Drains.Web.Bussines.Contracts.Types;
using Drains.Web.Bussines.Contracts.Types.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drains.Web.Bussines.Contracts.Services
{
    public interface IContractService
    {
        IContractModel GetAdressById(long id);
        IEnumerable<IContractModel> GetAll();
        IStatusResponse Create(int number, DateTime dateNow, DateTime dateEnd, long supplierId);
        IStatusResponse Delete(long id);
        IStatusResponse Update(long id, int number, DateTime dateNow, DateTime dateEnd, long supplierId);
    }
}
